package com.etermax.preguntados.androidCourse

import java.io.Serializable

data class Message(val value: String) : Serializable