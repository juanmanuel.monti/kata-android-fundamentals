package com.etermax.preguntados.androidCourse

import java.io.Serializable

data class RichTextMessage(val value: String) : Serializable