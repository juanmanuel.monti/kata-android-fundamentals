package com.etermax.preguntados.androidCourse

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatTextView

class MessageDisplayActivity: AppCompatActivity() {

    private lateinit var messageReceivedWidget: AppCompatTextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dispayed_message)

        intent.extras?.let { extras ->
            messageReceivedWidget = findViewById(R.id.received_message)
            val receivedMessage = extras.getSerializable(MESSAGE_KEY) as Message
            messageReceivedWidget.text = receivedMessage.value
        }
    }

    companion object {
        const val MESSAGE_KEY = "MESSAGE_KEY"

        fun newInstance(context: Context, message: Message): Intent {
            val bundle = Bundle()
            bundle.putSerializable(MESSAGE_KEY, message)
            val intent = Intent(context, MessageDisplayActivity::class.java)
            intent.putExtras(bundle)
            return intent
        }
    }
}