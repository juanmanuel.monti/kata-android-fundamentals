package com.etermax.preguntados.androidCourse

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatEditText

class MainActivity: AppCompatActivity() {

    private lateinit var messageInput: AppCompatEditText
    private lateinit var sendMessageButton: AppCompatButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        messageInput = findViewById(R.id.message_input_field)
        sendMessageButton = findViewById(R.id.send_message_button)

        sendMessageButton.setOnClickListener {

            val message = Message(value = messageInput.text.toString())
            val intentFactory = MessageDisplayActivity.newInstance(applicationContext, message)
            startActivity(intentFactory)
        }
    }
}